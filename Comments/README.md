Instructions
-----------------
To execute the sample, do as follows:

* Android
> Connect your Android device and run `./gradlew androidInstall`
* iOS
> Connect your iOS device and run `./gradlew launchIOSDevice`
* Desktop
> `./gradlew run`

Samples in this directory
---------------------------------
Comments
========

 A JavaFX Application that uses Gluon Charm: 

 - Charm Connect: the client-side counterpart of the Gluon Cloud service 
 - Charm Glisten: Simple application framework, custom UI controls and customized existing JavaFX UI controls
