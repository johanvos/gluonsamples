package com.gluonhq.demo.comments.views.home;

import com.gluonhq.charm.down.common.JavaFXPlatform;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.layout.layer.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.Swatch;
import com.gluonhq.demo.comments.model.Comment;
import com.gluonhq.demo.comments.CommentsFX;
import com.gluonhq.demo.comments.cloud.CommentsService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.ListView;
import javax.inject.Inject;

/**
 * FXML Controller class for home.fxml
 *
 */
public class HomePresenter implements Initializable {

    @FXML 
    private View homeView;
    
    // a ListView that will contain the comments
    @FXML
    private ListView<Comment> comments;
    
    // Injects an instance of CommentsService (as singleton)
    // See Afterburner.fx
    @Inject
    private CommentsService service;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        comments.setCellFactory((lv) -> new CommentListCell());
        comments.setPadding(new Insets(10.0));
        
        Platform.runLater(()->{
            homeView.setSwatch(Swatch.INDIGO);
            
            if(JavaFXPlatform.isDesktop()){
                homeView.getScene().getWindow().setWidth(400);
                homeView.getScene().getWindow().setHeight(600);
            }

            service.retrieveComments();
            comments.itemsProperty().bind(service.commentsProperty());
        });
        
        homeView.getLayers().add(new FloatingActionButton("Action", e -> {
            MobileApplication.getInstance().switchView(CommentsFX.COMMENTS_VIEW);
        }));
    }    
    
}
