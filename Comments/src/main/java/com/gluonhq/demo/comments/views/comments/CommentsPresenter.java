package com.gluonhq.demo.comments.views.comments;

import com.gluonhq.charm.glisten.application.MobileApplication;
import static com.gluonhq.charm.glisten.application.MobileApplication.HOME_VIEW;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.demo.comments.model.Comment;
import com.gluonhq.demo.comments.cloud.CommentsService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.inject.Inject;

public class CommentsPresenter implements Initializable {

    @FXML 
    private View commentsView;
    
    @FXML
    private TextField authorField;
    
    @FXML
    private TextArea commentsField;
    
    @FXML
    private Button submit;
    
    // Injects an instance of CommentsService (as singleton)
    // See Afterburner.fx
    @Inject
    private CommentsService service;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        // Enable submit only if both fields have content
        submit.disableProperty().bind(Bindings.createBooleanBinding(()->{
                return authorField.textProperty().isEmpty()
                        .or(commentsField.textProperty().isEmpty()).get();
            }, authorField.textProperty(),commentsField.textProperty()));
        
        // when this View is shown, move focus to the author field.
        commentsView.showingProperty().addListener((obs,b,b1)->{
            if(b1){
                commentsField.requestFocus();
                authorField.requestFocus();
            }
        });
    }
    
    /**
     * Clear input fields and return to Home_View
     */
    @FXML
    private void onCancel(){
        authorField.setText("");
        commentsField.setText("");
        MobileApplication.getInstance().switchView(HOME_VIEW);
    }
    
    /**
     * Add comment to the remote list. It will be added to the ListView, and also
     * it will go to the cloud, and broadcasted to the rest of clients.
     * Return to Home View
     */
    @FXML
    private void onSubmit(){
        service.addComment(new Comment(authorField.getText(), commentsField.getText()));
        authorField.setText("");
        commentsField.setText("");
        MobileApplication.getInstance().switchView(HOME_VIEW);
    }

}
