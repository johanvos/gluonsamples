package com.gluonhq.demo.comments.cloud;

import com.gluonhq.charm.connect.GluonClient;
import com.gluonhq.charm.connect.service.RemoteObservableList;
import com.gluonhq.charm.connect.service.StorageService;
import com.gluonhq.charm.connect.service.SyncFlag;
import com.gluonhq.demo.comments.model.Comment;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.annotation.PostConstruct;

/** Service to access the application on Gluon Cloud, retrieve a list with comments
 * and send new comments to the list.
 * 
 */
public class CommentsService {

    private GluonClient gluonClient;
    private StorageService storageService;
    
    /*
    Every list stored under the same application on the Cloud has a unique id:
    */
    private static final String CLOUD_LIST_ID = "comments";
    
    /*
    An observable wrapper of the retrieved list, used to expose it and bind the 
    ListView items to the list.
    */
    private final ObjectProperty<ObservableList<Comment>> commentsList = 
            new SimpleObjectProperty<>(FXCollections.<Comment>observableArrayList(),"comments"); 
    
    /**
     * See Afterburner.fx
     */
    @PostConstruct
    public void postConstruct() {
        gluonClient = GluonClientProvider.getGluonClient();
        storageService = gluonClient.getStorageService();
    }
    
    /**
     * Once there's a valid storageService, the contents of the list can be retrieved. This will return a 
     * RemoteObservableList. Note the flags:
     * - LIST_WRITE_THROUGH: Changes in the local list are reflected to the remote copy of that list on Gluon Cloud.
     * - LIST_READ_THROUGH: Changes in the remote list on Gluon Cloud are reflected to the local copy of that list
     *
     * This means that any change done in any client app will be reflected in the cloud, and inmediatelly broadcasted
     * to all the listening applications.
     */
    public void retrieveComments(){
        RemoteObservableList<Comment> retrieveList = storageService.<Comment>retrieveList(CLOUD_LIST_ID, Comment.class, 
                SyncFlag.LIST_WRITE_THROUGH, SyncFlag.LIST_READ_THROUGH);
        
        retrieveList.stateProperty().addListener((ov,s,s1)->{
            if(s1.equals(RemoteObservableList.State.SUCCEEDED)){
                commentsList.set(retrieveList);
            }
        });
        
    }
    
    /**  
     * Add a new comment to the list
     * Note comments can be deleted directly on the ListView, since its bounded to the list
     * @param comment
     */
    
    public void addComment(Comment comment){
        commentsList.get().add(comment);
    }
    
    /**
     *
     * @return: the wrapper of the remote list of comments.
     */
    public ObjectProperty<ObservableList<Comment>> commentsProperty(){
        return commentsList;
    }
    
}
