package com.gluonhq.demo.comments.views.home;

import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.control.ListTile;
import com.gluonhq.charm.glisten.visual.GlistenStyleClasses;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.demo.comments.model.Comment;
import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Custom format for ListCells on the ListView control
 * It uses ListTile control, with two graphic zones
 */
public class CommentListCell extends ListCell<Comment> {

    /**
     * Add a ListTile control to not empty cells
     * @param item the comment on the cell
     * @param empty 
     */
    @Override
    protected void updateItem(Comment item, boolean empty) {
        super.updateItem(item, empty);

        if (!empty && item != null) {
            ListTile tile = new ListTile();
            
            // First graphic area, on the left
            Label authorLabel = new Label(item.getAuthor());
            authorLabel.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 14.0));
            Label commentsLabel = new Label(item.getContent());
            tile.setPrimaryGraphic(new VBox(10.0,authorLabel,commentsLabel));
            
            // Second graphic area, on the right
            Button button = new Button(null, new Icon(MaterialDesignIcon.DELETE));
            button.getStyleClass().addAll(GlistenStyleClasses.BUTTON_ROUND, "myButton");
            button.setOnAction(e->showDialog(item));
            tile.setSecondaryGraphic(button);
            
//            setPadding(new Insets(5,10,5,10));
            setGraphic(tile);
        } else {
            setGraphic(null);
        }
    }
    
    /**
     * Create a Dialog for getting deletion confirmation
     * @param item 
     */
    private void showDialog(Comment item) {
        HBox title = new HBox(10);
        title.setAlignment(Pos.CENTER_LEFT);
        final Icon icon = new Icon(MaterialDesignIcon.WARNING);
        title.getChildren().add(icon);
        title.getChildren().add(new Label("Confirm deletion"));
        
        Dialog<ButtonType> dialog = new Dialog();
        dialog.setContent(new Label("This comment will be deleted permanently.\nDo you want to continue?"));
        dialog.setTitle(title);
        
        Button yes = new Button("Yes, delete permanently");
        yes.setOnAction(e->{dialog.setResult(ButtonType.YES); dialog.hide();});
        yes.setDefaultButton(true);
        Button no = new Button("No");
        no.setCancelButton(true);
        no.setOnAction(e->{dialog.setResult(ButtonType.NO); dialog.hide();});
        dialog.getButtons().addAll(yes,no);
        
        Platform.runLater(()->{
            Optional result = dialog.show();
            if(result.isPresent() && result.get().equals(ButtonType.YES)){
                /*
                With confirmation, delete the item from the ListView. This will be
                propagated to the Cloud, and from here to the rest of the clients
                */
                listViewProperty().get().getItems().remove(item);
            }
        });
    }

}
